package main

import (
	"bytes"
	"fmt"
	"html/template"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/alexdmoss/istio-sandbox/ossum-ui/koalas"
	log "github.com/sirupsen/logrus"

	"github.com/gorilla/mux"
)

const (
	srvAddr = ":8000"
)

type Koalas struct {
	Results *koalas.Results
}

var tpl = template.Must(template.ParseFiles("index.html"))

func main() {

	r := newRouter()

	go func() {
		log.Infof("server listening at %s", srvAddr)
		if err := http.ListenAndServe(srvAddr, r); err != nil {
			log.Panicf("error while serving: %s", err)
		}
	}()

	// allows clean handling of kill signals to the pid
	sigC := make(chan os.Signal, 1)
	signal.Notify(sigC, syscall.SIGTERM, syscall.SIGINT)
	<-sigC

}

func indexHandler(w http.ResponseWriter, r *http.Request) {

	koalaClient := &http.Client{Timeout: 10 * time.Second}
	koalaAPI := koalas.NewClient(koalaClient)

	results, err := koalaAPI.FetchKoalas()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	koalas := &Koalas{
		Results: results,
	}

	log.Infof("Koalas found: %+v", results) // @TODO: debug, can be removed

	buf := &bytes.Buffer{}
	err = tpl.Execute(buf, koalas)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	buf.WriteTo(w)

}

func healthHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "OK: 200")
}

func newRouter() *mux.Router {
	r := mux.NewRouter()
	r.HandleFunc("/healthz", healthHandler).Methods("GET")

	staticFileDirectory := http.Dir("./assets/")
	staticFileHandler := http.StripPrefix("/assets/", http.FileServer(staticFileDirectory))
	r.PathPrefix("/assets/").Handler(staticFileHandler).Methods("GET")

	r.HandleFunc("/", indexHandler).Methods("GET")

	return r
}
