# Traffic Management

First applied some default routes (`VirtualServices` and `DestinationRules`) for the `backend` and two `frontends` - the latter has subsets to handle the two versions pointing to the same `Service`.

The `VirtualService` for the `animals-api` is not technically needed, but it is advanced as [good practice](https://istio.io/latest/docs/ops/best-practices/traffic-management/#set-default-routes-for-services).

```sh
kubectl apply -f backend-default-routes.yaml
kubectl apply -f frontend-default-routes.yaml
```

---

## Test Routing Works

A basic Istio test - lets make sure all traffic can be sent to v1 or v2 of the frontend, regardless of number of `Pods` matching in the `Service`.

```sh
kubectl apply -f frontend-all-v1.yaml
```

This was easier to test with e.g:

```sh
i=0; while [[ $i -lt 10 ]]; do curl -s "https://ossum-ui.alexos.dev/" | grep 'API Version'; i=$(expr $i + 1); done
```

This did not work - I think because the source of `ossum-ui` is the ingress-controller, which calls the `Service` directly. In other words, we probably need the controllers to be in the mesh. I've had trouble doing this ...

Instead, lets try ingress via the Istio Gateway instead. In order to get a fixed IP the [Google docs](https://github.com/istio/istio/issues/1024#issuecomment-698193717) suggest patching the ingress-gateway service which is janky but will do for now:

```sh
# reserve IP via terraforming or gcloud to create the compute address, then ...
kubectl patch service istio-ingressgateway --patch '{"spec":{"loadBalancerIP": "104.199.19.53"}}' --namespace istio-system
# ... wait a few moments for it to take effect
```

With the supplied `frontend-gateway.yaml`, the gateway works fine (but over HTTP rather than HTTPS - not tried to configure that differently). However, I still get a roughly 50:50 split between v1 and v2.

I think this was due to labelling issues - `kustomize` + the way I've organised things was making this tricky. I rebuilt the deploy stage to allow easier separation and accurate labelling of things, then cleaned up the `frontend-all-v1.yaml` so that it configure the policy for the `VirtualService` used by the Istio Gateway instead of the default route I had set up.
104.199.19.53
This worked. Likewise it can be near-instantly flipped to all v2 with `frontend-all-v2.yaml`.

This gives the basic constructs of an A/B or blue/green or canary release strategy.

> Flagger automates the creation of a lot of elements of this and works with Istio objects - could be worth a look

@TODO: try again with nginx-ingress now that labels are fixed
 
### Header Based Routing

We can effectively feature-flag a new release - see `frontend-route-by-header.yaml`. With this applied, traffic is sent to the v2 frontend if the header `"beta: true"` is set. Prefix & Regex matching are also possible.

---

## Operability

With all requests going to v2 of the frontend - i.e. making an API call - we can more easily start to experiment with some of the other operability features.

### Fault Injection

To begin with, lets inject some faults into the API call being made. The `backend-default-routes.yaml` was replaced with `backend-500-injection.yaml` looking like this:

```yaml
---
apiVersion: networking.istio.io/v1beta1
kind: VirtualService
metadata:
  name: animals-api-route
  namespace: animals
spec:
  hosts:
  - animals-api
  http:
  - fault:
      abort:
        httpStatus: 500
        percentage:
          value: 50
    route:
    - destination:
        host: animals-api
```

With this config, half our requests to the API fail. I validated this via the UI app (only half the calls worked), and by calling directly `kubectl run -it diag --image=mosstech/diag-tools:latest /bin/bash` and then curling the API directly).
