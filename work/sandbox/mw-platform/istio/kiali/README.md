# kiali

Install guide recommends using their bash script - do so with caution!

Grab it from: `curl -sLo kiali-operator-install.sh https://kiali.io/getLatestKialiOperator`.

I installed the operator only with the following:

```sh
./kiali-operator-install.sh \
    --operator-install-kiali false \
    --operator-namespace kiali-operator \
    --operator-cluster-role-creator true \
    --operator-view-only-mode true \
    --operator-watch-namespace kiali
```

This seemed to do the trick - but is pretty magical, and I haven't examined its action too closely.

> I had to add the `operator-cluster-role-creator` option due to my use of Kiali across all namespaces

Used the [example CR](https://raw.githubusercontent.com/kiali/kiali-operator/master/deploy/kiali/kiali_cr.yaml) to put together config to apply.

From here, we can then:

```sh
kubectl create ns kiali
kubectl apply -f kiali.yaml
```

After a few seconds things should start appearing in there - check the operator logs if not.
