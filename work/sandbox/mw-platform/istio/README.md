

## Trying Kiali

> Replaced this with Operator install - see `./control-plane/istio/kiali/`. Works much better

`curl -sLo kiali.yaml https://raw.githubusercontent.com/istio/istio/release-1.8/samples/addons/kiali.yaml` and have a poke around. The majority of this is a CRD for `MonitoringDashboard` and then a bunch of these resources. It also consists of a Deployment, ConfigMap, SA and some ClusterRoles - one of which is pretty permissive (lots of permissions on Istio objects, and can patch most other things). It does also create a read-only Role but it isn't used, which may come up later!

The relevant resources deploy into the `istio-system` namespace, so I applied the whole thing. Somewhat comically it failed first time due to the reconciliation time between the CRD and the subsequent `MonitoringDashboard` objects being known to the apiserver.

> Beware. The monitoring dashboards end up in the default namespace!

Somewhat surprisingly given PSP, it started up fine, logs were quiet, although this is of course eye-catching and we'll need so solve:

```log
I0108 13:10:46.586598       1 kiali.go:153] Using authentication strategy [anonymous]
W0108 13:10:46.586709       1 kiali.go:155] Kiali auth strategy is configured for anonymous access - users will not be authenticated.
```

`istioctl dashboard kiali` is I think a shortcut to port-forwarding so that `http://localhost:20001/kiali` works. No login as expected from the above - see all the namespaces. Generally browsing around it is whinging a lot about not being able to connect to Prometheus so this may be a pre-req for a lot of its visualisation functionality, which makes some sense of course. It also tells me off a little for not setting the version label on deployments.

Grabbed a screenshot of its overview of the workload I opted in to illustrate:

![Kiali Overview for a workload](kiali-1.png)

There are views to access metrics and traces, but without those components installed it's not doing anything really.

It's slightly irritating that it is telling me about errors/warnings with certain "istio config" but clicking through takes you to screens with no mention of these whatsoever.

The ability to use this console to create traffic policy and enable/disable mesh sidecar injection - i.e. change things! - should ring alarm bells for anyone reading this. Our use-case here is a read-only visualisation tool so that needs looking into. We would be able to put an ingress in front of it to solve for authentication, but this definitely needs sorting if we wanted it to be usable by teams.

My strong suspicion is that with no default `VirtualService` definitions and no connectivity to Prometheus or Jaeger, Kiali's value is limited.

Poking around a bit more, the noddy install option above from the Istio docs can be superceded by a full [Kiali operator](https://kiali.io/documentation/v1.28/installation-guide/#_install_kiali_latest), which has more configuration options, and looks more reassuring. Some features of the operator include:

- include/exclude namespaces to monitor, including via selectors
- read-only mode (although the docs imply it retains its privileged ClusterRole but just hides this in the UI)
- authentication options such as kubernetes' RBAC with service-account [tokens](https://kiali.io/documentation/v1.28/configuration/authentication/token/) (the default) or [OpenID Connect](https://kiali.io/documentation/v1.28/configuration/authentication/openid/) (via something like [kube-oidc-proxy](https://github.com/jetstack/kube-oidc-proxy)) if we wanted to go down this route

Role-based controls are possible, with the openid or token authentication mechanisms [authorising based on Kubernetes' RBAC](https://kiali.io/documentation/v1.28/configuration/rbac/). My simple cluster doesn't have all the RBAC complexity of JLDP but the docs sound reasonable - it'll be interesting to see how this works for our namespace-admin ClusterRole stuff. It _should_ be fine if we are ok with people being able to see everything but not change anything (because we'll run it in read-only mode).

> Given our desire to use this as just a read-only thing, it is _probably_ a lot easier to just stick an Octopus-SSO'd ingress in front of this thing and be done with it, tbh, but the options are there

I'm going to come back to this with the operator install after unpacking the prometheus/jaeger stuff, just so the tool feels more useful - but it looks viable.

## Prometheus

Lets be lazy to start with and install the out-the-box standalone Prometheus just to see what we've got / confirm metrics etc: `curl -sLo prometheus.yaml https://raw.githubusercontent.com/istio/istio/release-1.8/samples/addons/prometheus.yaml`

This is a very vanilla Prometheus that goes into the `istio-system` namespace.

After getting it up and running, it looks to work a bit differently to what we're used to. The Prometheus provided is configured to scrape pods with the following annotations, which Istio puts there:

```yaml
    prometheus.io/path: /stats/prometheus
    prometheus.io/port: "15020"
    prometheus.io/scrape: "true"
```

In their Prometheus, this materialises under a target job of `kubernetes-pods` (each pod being an endpoint under this).

This felt a bit weird, and we will likely have something like VictoriaMetrics or Thanos in place, so needing to run a separate Istio-supplied (non-Operator) Prometheus is just odd. So I decided to fiddle around a bit.

> It's interesting that a lot of the advice out there from quick googling is focused on Istio's Mixer component - which is now gone :)

### What about scraping Envoy directly?

I first created a service + servicemonitor to scrape Envoy directly, which listens on 15090. In retrospect could've done a PodMonitor but w/e.

For Envoy its port is `15090`, with the scrape on `/stats/prometheus`.

This worked fine once I realised the Istio Prometheus had overwritten some of my cluster-scoped RBAC for my "main" Prometheus. Oops.

When scraping Envoy directly like this, you see more clearly how the metrics work.

```t
istio_requests_total{
  connection_security_policy="none",
  destination_app="moss-work",
  destination_canonical_revision="latest",
  destination_canonical_service="moss-work",
  destination_principal="unknown",
  destination_service="moss-work-metrics.moss-work.svc.cluster.local",
  destination_service_name="moss-work-metrics",destination_service_namespace="moss-work",destination_version="unknown",destination_workload="moss-work",
  destination_workload_namespace="moss-work",
  endpoint="istio",
  instance="10.224.0.31:15090",
  job="moss-work",
  namespace="moss-work",
  pod="moss-work-84c4dc5d9c-7xq8t",
  reporter="destination",
  request_protocol="http",
  response_code="200",
  response_flags="-",
  service="moss-work-istio",
  source_app="unknown",
  source_canonical_revision="latest",
  source_canonical_service="unknown",
  source_principal="unknown",
  source_version="unknown",
  source_workload="unknown",
  source_workload_namespace="unknown"}
```

The cardinality looks similar to what we would see with Traefik to be honest. The `source_*` fields not being populated is interesting, and presumably a consequence of my ingress-controller not being part of the mesh. One to try.

Note that both request & response metrics are present:

![Istio's Envoy metrics](istio-envoy-metrics.png)

How does that compare to Traefik?

![Traefik's metrics](traefik-metrics.png)

(I believe in our case entrypoint vs backend in Traefik is largely the same thing - as the frontend does nothing more than proxy the request)

Both offer request duration (although Istio measures this in milliseconds).

Istio also offers request & response bytes.

We don't appear to make use of the traefik open_connections or server_up metrics. Whilst these don't appear under `istio_*`, they do appear to be there under `envoy_*` (which has a shedload more!).

> Given the vast array of envoy out-the-box metrics we may want to review and trim these down to avoid wastage.

### What about scraping istiod?

With the advent of telemetry-v2 in Istio - all the metrics are on Envoy instead (other than metrics about the control plane itself).

I decided to create a ServiceMonitor for istiod itself just to see what was there, and indeed after scraping /metrics on port 15014, we only see `pilot_*` metrics appearing in Prometheus.

I found an [article by Datadog](https://www.datadoghq.com/blog/how-to-monitor-istiod/) that helps make the role of these metrics clearer, and may provide some inspiration when we come to do this stuff properly.

This doesn't explain why there are more metrics in the Istio-supplied Prometheus than scraping Envoy directly. Further poking around made this clearer to me - I was scraping the wrong port! 15020 is the right one (whatever I read that said 15090 or 15000 was wrong). The merged metrics is mentioned tangentially in the [docs too](https://istio.io/latest/docs/ops/deployment/requirements/#ports-used-by-istio).

This snippet was quite useful while debugging this - just a little wrap to get a scrape back to your terminal easily:

```sh
kubectl exec "$(kubectl get pod -l app=moss-work -o jsonpath='{.items[0].metadata.name}')" -c istio-proxy -- curl 'localhost:15020/stats/prometheus'
```

The `istio-proxy` does not name the 15020 port in its config, which makes `PodMonitor` a challenge - it expects a named port (`targetPort` as an integer is allowed, but deprecated). I therefore created a `Service` + `ServiceMonitor` so I could name the port explicitly.

## BookInfo Sample

Decided to install the sample app at this point to fully stretch out the metrics, as my simple app wasn't cutting it - want to know the outbound metrics are working properly!

```sh
curl -sLo bookinfo.yaml https://raw.githubusercontent.com/istio/istio/master/samples/bookinfo/platform/kube/bookinfo.yaml
kubectl apply -f bookinfo.yaml
```

This did get the inbound/outbound metrics in the Istio shipped dashboards working. It worked even without the destination rules - we see calls out from productpage to reviews/ratings.

## Sidecar with nginx-ingress-controller

Add annotations to controller:

```yaml
traffic.sidecar.istio.io/includeInboundPorts: ""
traffic.sidecar.istio.io/excludeInboundPorts: "80,443"
# substitute API_SERVER_IP_RANGE from $(kubectl get svc kubernetes -n default -o jsonpath='{.spec.clusterIP}')/32
traffic.sidecar.istio.io/excludeOutboundIPRanges: __API_SERVER_IP_RANGE__
```

Also for each `Ingress` definition ... /sigh:

```yaml
kubernetes.io/ingress.class: nginx
nginx.ingress.kubernetes.io/service-upstream: "true"
nginx.ingress.kubernetes.io/upstream-vhost: <service>.<namespace>.svc.cluster.local
```

> We might be able to mutate this in, but note the need to know the service name.

Actually this may only be needed for ingress in a different namespace to the service being targeted.

---

## To Do

- [x] Kiali
  - [x] read-only view
  - [x] re-evaluate once it can talk to prometheus (and maybe jaeger)
  - [ ] fiddle with the auth-n and auth-z options
- [x] Metrics -> Prometheus
  - [x] Default installation works fine but we would probably not do this
  - [x] Integrate with our prometheus instead
    - [x] Try scraping Envoy directly
    - [x] Try scraping istiod collection
  - [x] How do metrics compare to Traefik?
    - [x] Can Istio tell response duration?
  - [x] Try getting source / request / response all populated
    - [ ] See if can get source/destination metrics working between nginx-ingress and a workload instead of relying on bookinfo
- [x] Try the out-the-box Istio dashboards
- [ ] Check need for app/version labelling *Kiali whinges at you if you don't! So what?*
- [ ] Access Logging - `kubectl exec -it xtremecloud-sso-minikube-0 -c istio-proxy -- sh -c 'curl -k -X POST localhost:15000/logging?level=debug'`
- [ ] Istio ingress gateway instead of nginx?
- [ ] Custom profile to remove the egress gateway / default + add ingress
- [ ] What does a default VirtualService for all look like?
- [ ] Try a bit of canarying
- [ ] Try a bit of fault injection
- [ ] What chaos does mTLS cause?
- [ ] Think about how to measure cost & performance overheads
