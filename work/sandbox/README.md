# istio-sandbox

## Migrating from Personal Repo

- [ ] kiali
- [ ] notes on scraping
- [ ] nginx-ingress notes/failure
- [ ] migrate API to hoff-api in platform-apps
- [ ] rework platform-health to call api
- [ ] demo dashboard
- [ ] build list of other features to try

## Operator Install

You will need `istioctl` installed. We should add this to `jldp-deploy-tools` also.

You _can_ do `istioctl operator init` and let the magic unfurl before your very eyes. What I've done here to give a little more illusion of control is:

`istioctl operator dump > ./control-plane/istio-operator.yaml`, reviewed and applied. This should create an `istio-operator` namespace with a single Pod living in it.

- did this manually in Verify. Obviously we would do in CI. Needs high privileges - clusterrole/namespace creation - but `clusters` should have it
- the `dump` actually puts the Namespace part way down the list, meaning this needed running twice :facepalm:
- we're not overriding the default namespaces for the operator or istio itself, because why would we. Note that with the CNI install (see below) you get a component in `kube-system` also
- there are a number of [options](https://istio.io/latest/docs/reference/commands/istioctl/#istioctl-operator-init) to this command if needed

## istiod Install

Done using operator - i.e. the `kind: IstioOperator` using `istio-with-cni.yaml`.

> Because we use GKE with CNI, some [custom stuff](https://istio.io/latest/docs/setup/additional-setup/cni/#hosted-kubernetes-settings) is needed. 

It is installed with: `istioctl install -f ./control-plane/istio-with-cni.yaml`.

- bootstrap of a PSP/RBAC needed for CNI (see `kubectl apply -f ./control-plane/rbac/`). I used one from the interwebz (:yeehaw:) - its pretty loose and we should look to tighten
- using `default` profile - can be customised later, but for now we only need the istio core + ingress-gateway to fiddle with

## Enabling Sidecar

Either label the namespace, then opt workloads **out** ...

```sh
kubectl label namespace "${ns}" istio-injection=enabled --overwrite=true
kubectl annotate --overwrite pods -l=app=<app-to-ignore> sidecar.istio.io/inject='false'
```

... or just opt **in** the worloads you want:

```sh
kubectl annotate --overwrite pods -l=app=<app-to-mesh> sidecar.istio.io/inject='true'
```

Pods must be deleted and recreated for the injector to pick it up, of course.

> You could also use `istioctl kube-inject` I suppose. The only sort of scenario I could imagine this being useful is if you explicitly wanted to store the fully-modified manifest, i.e. something like `istioctl kube-inject -f deployment.yaml -o deployment-injected.yaml`. Not tried it.
