# dashboard

Dashboard saved as `istio-websites.json` using the following metrics.

---

> Note "unknown" source due to ingress not being in mesh

Workload variable: `query_result((sum(istio_requests_total) by (destination_workload) or sum(istio_requests_total) by (source_workload)))`, then regex with `/.*workload="([^"]*).*/`

---

## 4 Golden Signals

4GS Traffic:

Original - `sum(rate(nginx_http_requests_total{pod=~"$service-.*",status!=""}[5m])) by (status)`

> Caution is needed here, due to the sudden appearance of inbound + outbound metrics for both the UI & API
> Count by the name of the UI Pod would double count, due to the inbound + outbound metric from it
> Count by just destination would double count, as it would pick up destination for both UI & API
> Suggest using reporter= to help filter

New - `sum(rate(istio_requests_total{destination_workload="$workload", reporter="destination", response_code!=""}[5m])) by (response_code)`

> To pick up the requests to the API, change destination_workload="animals-api" (with the load generator, this is half the requests)

4GS Latency:

Original - `histogram_quantile(0.5,max(increase(nginx_http_request_duration_seconds_bucket{pod=~"$service-.*"}[5m])) by (le))`

New - `(histogram_quantile(0.50, sum(irate(istio_request_duration_milliseconds_bucket{reporter="destination",  destination_workload="$workload"}[5m])) by (le)) / 1000)`

> As above, we have to adapt slightly to deal with the inbound & outbound metrics. We also need to convert out of the milliseconds unit

4GS Availability:

Original - `sum(rate(nginx_http_requests_total{pod=~"$service-.*",code!~"5.*"}[1m])) / sum(rate(nginx_http_requests_total{pod=~"$service-.*"}[1m]))`

New - `sum(rate(istio_requests_total{destination_workload="$workload", reporter="destination", response_code!~"[45].*"}[1m])) / sum(rate(istio_requests_total{reporter="destination", destination_workload="$workload"}[1m]))`

> A similar conversion to that needed for the Traffic graph

4GS % Errors:

Original - `sum(rate(nginx_http_requests_total{pod=~"$service-.*",status="500"}[5m])) / sum(rate(nginx_http_requests_total{pod=~"$service-.*"}[5m]))`

New - `sum(rate(istio_requests_total{destination_workload="$workload", reporter="destination", response_code="500"}[5m])) / sum(rate(istio_requests_total{reporter="destination", destination_workload="$workload"}[5m]))`

> And again a similar conversion - these are repeated for each error code we're interested in

> We could group by source workload here also, as they do on the Istio-supplied dashboard

---

That covers our 4GS dashboard. What else does Istio's workload dashboard ship with though?

---

## Inbound

A chart showing incoming requests grouped by their Source + Response Code. This is effectively the same as our Traffic graph, but adding the Source dimension on top - where it is known to the Mesh at least.

> For our UI, because the ingress-controller is not in the mesh, the source will appear as "Unknown". But when looking at this for the API call, we should see the source as the UI (which is in the mesh)

Slightly simplified version from the supplied dashboard:

`round(sum(irate(istio_requests_total{destination_workload=~"$workload", reporter="destination", source_workload=~"$srcwl"}[5m])) by (source_workload, response_code), 0.001)`

Incoming Request Success - which is effectively just an inversion of our %errors, so skipped. Note that it does Group by Source though.


Incoming Req Duration by Source - which is the same as our latency dashboard, but again grouping by Source.

Incoming Req by Size - this is an addition, and perhaps interesting: `histogram_quantile(0.50, sum(irate(istio_request_bytes_bucket{reporter="destination", destination_workload=~"$workload"}[1m])) by (source_workload, le))`

And likewise Response Size by Source: `histogram_quantile(0.50, sum(irate(istio_response_bytes_bucket{reporter="destination", destination_workload=~"$workload"}[1m])) by (source_workload, le))`

---

## Outbound

These are new in respect to what we currently do with Traefik, and are broadly similar. The client having a view on their upstreams feels valuable.

Note the change to use the `reporter="source"` and `source_workload`, grouping by `destination_workload` instead.

Outbound by Dest / Code: `round(sum(irate(istio_requests_total{source_workload=~"$workload", reporter="source"}[5m])) by (destination_workload, response_code), 0.001)`

Outbound Success: `sum(irate(istio_requests_total{reporter="source", source_workload=~"$workload", response_code!~"5.*"}[5m])) by (destination_workload) / sum(irate(istio_requests_total{reporter="source", source_workload=~"$workload"}[5m])) by (destination_workload)`

Outbound Duration by Destination: `(histogram_quantile(0.50, sum(irate(istio_request_duration_milliseconds_bucket{reporter="source", source_workload=~"$workload"}[1m])) by (destination_workload, le)) / 1000)`

Outbound Request Size by Dest: `histogram_quantile(0.50, sum(irate(istio_request_bytes_bucket{reporter="source", source_workload=~"$workload"}[1m])) by (destination_workload, le))`

Response Size by Dest: `histogram_quantile(0.50, sum(irate(istio_response_bytes_bucket{reporter="source", source_workload=~"$workload"}[1m])) by (destination_workload, le))`
