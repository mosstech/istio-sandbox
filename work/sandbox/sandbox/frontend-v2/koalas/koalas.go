package koalas

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"

	log "github.com/sirupsen/logrus"
)

type Client struct {
	http *http.Client
}

type Results struct {
	Status       string `json:"status"`
	Version      string `json:"apiVersion"`
	TotalResults int    `json:"totalResults"`
	Message      string `json:"message"`
}

func NewClient(httpClient *http.Client) *Client {
	return &Client{httpClient}
}

func (c *Client) FetchKoalas() (*Results, error) {

	koala_api_url := os.Getenv("KOALA_API_URL")
	if koala_api_url == "" {
		koala_api_url = fmt.Sprintf("http://localhost:8080/api/v1/koalas")
	}

	log.Infof("Calling endpoint: %s", koala_api_url) // @TODO: debug - can remove
	resp, err := c.http.Get(koala_api_url)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf(string(body))
	} else {
		log.Infof("Return code from API call was: %v", resp.StatusCode) // @TODO: debug - can remove
	}

	res := &Results{}
	err = json.Unmarshal(body, res)
	if err != nil {
		log.Errorf("Error unmarshalling JSON: %v", err)
	}

	return res, err

}
