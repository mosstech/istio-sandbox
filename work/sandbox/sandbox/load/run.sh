#!/usr/bin/env bash
set -euo pipefail

if [[ ${1:-} == "--loop" ]]; then
    duration=0
else
    duration=60s
fi

vegeta attack \
    -duration=${duration} \
    -rate=10/s \
    -targets=target.list \
    -timeout=5s \
    -output="output/vegeta.bin"

vegeta plot -title="Ossum UI" output/vegeta.bin > output/vegeta.html
