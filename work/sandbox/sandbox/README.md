# istio-sandbox

A set of apps and config for me to experiment with Istio features, outside of the safety of the BookInfo sample app that ships with Istio itself.

Some inspiration for ideas from [istiobyexample.dev](https://www.istiobyexample.dev/) as well as the [Istio docs](https://istio.io/latest/docs/tasks/) themselves.

---

## Usage

Each app ships with a Makefile. For development you may also wish to `go get github.com/pilu/fresh` for your `$GOPATH` and then you can benefit from hot reloads by just running `fresh`.

---

## Story to Tell

0. Cluster with Istio deployed
1. Have a basic frontend app
2. Add integration with a backend app
3. Show off the Istio metrics & kiali
4. Manage traffic to that backend app with Istio - canary it
   - could route by header / mirror for more critical
5. Inject faults into that call
   - show in dashboard
   - circuit break the failing call
6. Fix the faults but add occasional delay
7. Add retries to handle the delay

---

## The Plan

- [x] Basic frontend app
- [x] Integrate the above with basic backend app
- [x] CI pipeline to deploy both to separate namespaces
- [x] Validate Envoy injection
- [x] Add a Datastore call to show some value when calling outside kube
- [ ] Istio features to explore:
  - [ ] Observability
    - [x] *Kiali tweaks in mw-platform* Topology / service graphing
    - [x] In/out metrics - build a dashboard
      - [ ] Could do with the charts cleaning up a bit - e.g. axes/colours
      - [ ] Inbound/Outbound latency from ossum-ui looks suspiciously similar
    - [ ] Access logging
    - [ ] Distributed trace collection
  - [ ] Operability
    - [x] Route by header
    - [x] Canarying
      - [ ] Try flagger to handle more of this
    - [ ] Fault injection
    - [ ] Delay injection
    - [ ] Request timeouts
    - [ ] Circuit breaking
    - [ ] Retries
    - [ ] Traffic mirroring
    - [ ] ServiceEntries for Google APIs
  - [ ] Security
    - [ ] Configure the istio gateway with cert-manager
    - [ ] mTLS
    - [ ] validate JWTs
    - [ ] Auth policies
    - [ ] Third party traffic monitoring

---

## Golang Fun

I'm not great with Golang, so a few objectives for me while building these apps too:

- [ ] Move tests out of root - is Go too opinionated for this?
- [ ] Watch Tests equivalent
- [ ] Proper logging
- [ ] Test coverage
- [x] Clean close of app
