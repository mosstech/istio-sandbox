# hoff-api

“I think people respect me because they feel like - I'm kind of like Christmas. I come back every year. You can't get rid of me. I just keep coming back.”

---

This is a _very_ basic API deployed into the `platform-apps` namespace to facilitate testing by Team Nimbus of platform features before they hit primetime. It compliments/is called by the `platform-health` microservice, which is more frontend-shaped in nature.

---

## To Do

- [ ] Basic App working
- [ ] Deploy to CI and check works in-cluster
- [ ] Ensure tests are respectable
- [ ] Observability things - logging, metrics, etc.
- [ ] Add something like a GCS/Datastore call to actually bring back pictures of The Hoff
- [ ] Bring into the mesh
- [ ] Move to `Microservice` (dependent on Istio compatibility)
