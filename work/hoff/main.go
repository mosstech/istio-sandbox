package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	log "github.com/sirupsen/logrus"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
)

const (
	srvAddr = ":8080"
)

type Results struct {
	Status       string `json:"status"`
	ApiVersion   string `json:"apiVersion"`
	TotalResults int    `json:"totalResults"`
	Message      string `json:"message"`
}

func init() {
	log.SetFormatter(&log.JSONFormatter{})
	log.SetOutput(os.Stdout)
}

func main() {

	router := mux.NewRouter()

	router.MethodNotAllowedHandler = http.HandlerFunc(sendNotFound)
	router.NotFoundHandler = http.HandlerFunc(sendBadMethod)

	router.HandleFunc("/healthz", healthHandler).Methods("GET")
	router.HandleFunc("/api/v1/hoffme", hoffHandler).Methods("GET")

	methodsOk := handlers.AllowedMethods([]string{"GET"})

	go func() {
		log.Infof("server listening at %s", srvAddr)
		if err := http.ListenAndServe(srvAddr, handlers.CORS(methodsOk)(router)); err != nil {
			log.Panicf("error while serving: %s", err)
		}
	}()

	// allows clean handling of kill signals to the pid
	sigC := make(chan os.Signal, 1)
	signal.Notify(sigC, syscall.SIGTERM, syscall.SIGINT)
	<-sigC

}

func healthHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "OK: 200")
}

func hoffHandler(w http.ResponseWriter, r *http.Request) {
	log.Infof("Hoff API was called") // @TODO: debug, remove
	version := readVersionFile()
	// @TODO: more quotes or images!
	sendResponse("Ninety-nine percent of people now call me The Hoff - and it's out of respect.", version, w, r)
}

func sendResponse(response string, version string, w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	out, err := json.Marshal(Results{
		Status:       "ok",
		ApiVersion:   version,
		TotalResults: 1,
		Message:      response,
	})
	if err != nil {
		panic(err)
	}
	fmt.Fprint(w, string(out))

	return
}

func sendNotFound(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusNotFound)
	str := `{"Error":"Path not allowed"}`
	fmt.Fprint(w, str)
	return
}

func sendBadMethod(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusMethodNotAllowed)
	str := `{"Error":"Method not allowed"}`
	fmt.Fprint(w, str)
	return
}

func readVersionFile() string {
	b, err := ioutil.ReadFile(".apiversion")
	if err != nil {
		fmt.Print(err)
	}
	return string(b)
}
